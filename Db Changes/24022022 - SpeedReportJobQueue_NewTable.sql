﻿CREATE TABLE [dbo].[SpeedReportJobQueue] (
    [Id]           INT              IDENTITY (1, 1) NOT NULL,
    [Email]        NVARCHAR (1000)  NULL,
    [SiteUrl]      NVARCHAR (400)   NOT NULL,
    [Location]     INT              NOT NULL,
    [JobGuid]      UNIQUEIDENTIFIER NOT NULL,
    [Status]       NVARCHAR (400)   NOT NULL,
    [TestUrl]      NVARCHAR (400)   NULL,
    [ReportUrl]    NVARCHAR (400)   NULL,
    [ErrorMessage] NVARCHAR (1000)  NULL,
    [CreatedOnUtc] DATETIME2 (7)    NOT NULL,
    CONSTRAINT [PK_SpeedReportJobQueue] PRIMARY KEY CLUSTERED ([Id] ASC)
);

