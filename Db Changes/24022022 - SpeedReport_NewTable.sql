﻿CREATE TABLE [dbo].[SpeedReport] (
    [Id]                     INT             IDENTITY (1, 1) NOT NULL,
    [ReportJobQueueId]       INT             NOT NULL,
    [GtMetrixGrade]          NVARCHAR (400)  NOT NULL,
    [PerformanceScore]       INT             NOT NULL,
    [StructureScore]         INT             NOT NULL,
    [HtmlBytes]              INT             NOT NULL,
    [PageBytes]              INT             NOT NULL,
    [PageRequests]           INT             NOT NULL,
    [RedirectDuration]       INT             NOT NULL,
    [ConnectDuration]        INT             NOT NULL,
    [BackendDuration]        INT             NOT NULL,
    [TimeToFirstByte]        INT             NOT NULL,
    [FullyLoadedTime]        INT             NOT NULL,
    [SpeedIndex]             INT             NOT NULL,
    [LargestContentfulPaint] INT             NOT NULL,
    [TimeToInteractive]      INT             NOT NULL,
    [TotalBlockingTime]      INT             NOT NULL,
    [CumulativeLayoutShift]  DECIMAL (18, 4) NOT NULL,
    [Har]                    NVARCHAR (MAX)  NULL,
    CONSTRAINT [PK_SpeedReport] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_SpeedReport_ReportJobQueueId_SpeedReportJobQueue_Id] FOREIGN KEY ([ReportJobQueueId]) REFERENCES [dbo].[SpeedReportJobQueue] ([Id])
);


GO
CREATE NONCLUSTERED INDEX [IX_SpeedReport_ReportJobQueueId]
    ON [dbo].[SpeedReport]([ReportJobQueueId] ASC);

