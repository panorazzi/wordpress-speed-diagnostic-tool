﻿using System;

namespace Nop.Core.Domain.SpeedDiagnostic
{
    /// <summary>
    /// Represents a recurring payment
    /// </summary>
    public partial class SpeedReport : BaseEntity
    {
        public int ReportJobQueueId { get; set; }

        public string GtMetrixGrade { get; set; }

        public int PerformanceScore { get; set; }

        public int StructureScore { get; set; }

        public int HtmlBytes { get; set; }

        public int PageBytes { get; set; }

        public int PageRequests { get; set; }

        public int RedirectDuration { get; set; }

        public int ConnectDuration { get; set; }

        public int BackendDuration { get; set; }

        public int TimeToFirstByte { get; set; }

        public int FullyLoadedTime { get; set; }

        public int SpeedIndex { get; set; }

        public int LargestContentfulPaint { get; set; }

        public int TimeToInteractive { get; set; }

        public int TotalBlockingTime { get; set; }

        public decimal CumulativeLayoutShift { get; set; }

        public string Har { get; set; }

    }
}
