﻿using System;

namespace Nop.Core.Domain.SpeedDiagnostic
{
    /// <summary>
    /// Represents a product
    /// </summary>
    public partial class SpeedReportJobQueue : BaseEntity
    {
        public SpeedReportJobQueue()
        {
            JobGuid = Guid.NewGuid();
        }

        public string Email { get; set; }

        public Guid JobGuid { get; set; }

        public string SiteUrl { get; set; }

        public int Location { get; set; }

        public string Status { get; set; }

        public string TestUrl { get; set; }

        public string ReportUrl { get; set; }

        public string ErrorMessage { get; set; }

        public DateTime CreatedOnUtc { get; set; }

    }
}