﻿using System.Data;
using FluentMigrator.Builders.Create.Table;
using Nop.Core.Domain.SpeedDiagnostic;
using Nop.Data.Extensions;

namespace Nop.Data.Mapping.Builders.SpeedDiagnostic
{
    public partial class SpeedReportBuilder : NopEntityBuilder<SpeedReport>
    {
        #region Methods

        /// <summary>
        /// Apply entity configuration
        /// </summary>
        /// <param name="table">Create table expression builder</param>
        public override void MapEntity(CreateTableExpressionBuilder table)
        {
            table
                .WithColumn(nameof(SpeedReport.GtMetrixGrade)).AsString(400).NotNullable()
                .WithColumn(nameof(SpeedReport.ReportJobQueueId)).AsInt32().ForeignKey<SpeedReportJobQueue>(onDelete: Rule.None);
        }

        #endregion
    }
}