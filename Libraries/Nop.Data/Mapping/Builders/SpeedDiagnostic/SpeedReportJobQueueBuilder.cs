﻿using FluentMigrator.Builders.Create.Table;
using Nop.Core.Domain.SpeedDiagnostic;

namespace Nop.Data.Mapping.Builders.SpeedDiagnostic
{
    /// <summary>
    /// Represents a product entity builder
    /// </summary>
    public partial class SpeedReportJobQueueBuilder : NopEntityBuilder<SpeedReportJobQueue>
    {
        #region Methods

        /// <summary>
        /// Apply entity configuration
        /// </summary>
        /// <param name="table">Create table expression builder</param>
        public override void MapEntity(CreateTableExpressionBuilder table)
        {
            table
                .WithColumn(nameof(SpeedReportJobQueue.SiteUrl)).AsString(400).NotNullable()
                .WithColumn(nameof(SpeedReportJobQueue.Status)).AsString(400).NotNullable()
                .WithColumn(nameof(SpeedReportJobQueue.TestUrl)).AsString(400).Nullable()
                .WithColumn(nameof(SpeedReportJobQueue.ReportUrl)).AsString(400).Nullable()
                .WithColumn(nameof(SpeedReportJobQueue.Email)).AsString(1000).Nullable()
                .WithColumn(nameof(SpeedReportJobQueue.ErrorMessage)).AsString(1000).Nullable();
        }

        #endregion
    }
}