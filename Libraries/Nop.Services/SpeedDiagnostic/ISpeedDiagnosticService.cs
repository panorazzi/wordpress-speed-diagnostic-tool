﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Nop.Core.Domain.SpeedDiagnostic;
using System;
using Nop.Core;

namespace Nop.Services.SpeedDiagnostic
{
    /// <summary>
    /// Blog service interface
    /// </summary>
    public partial interface ISpeedDiagnosticService
    {
        string RetrieveGTMetrixLocation(int locationId);

        Task InsertSpeedReportJobQueueAsync(SpeedReportJobQueue job);

        Task<SpeedReportJobQueue> GetJobQueueByIdAsync(int jobId);

        IList<SpeedReportJobQueue> GetAllPendingJobQueue();

        IList<SpeedReportJobQueue> GetAllRunningJobQueue();

        Task<IPagedList<SpeedReportJobQueue>> GetAllJobQueueAsync(string siteUrl = null,
            string email = null, int locationId = 0, string status = null, int pageIndex = 0,
            int pageSize = int.MaxValue, bool showHidden = false);

        Task UpdateSpeedReportJobQueueAsync(SpeedReportJobQueue job);

        Task<SpeedReportJobQueue> GetSpeedReportJobQueueByGuidAsync(Guid jobGuid);

        Task<string> GenerateUrlAsync(SpeedReportJobQueue job);

        Task InsertSpeedReportAsync(SpeedReport report);

        Task<SpeedReport> GetSpeedReportByIdAsync(int reportId);

        SpeedReport GetSpeedReportByJobIdAsync(int jobId);

        Task<(bool success, string report)> GetGTMetrixResultAsync(string jobGuid);

        Task<int> CheckResultStatusAsync(string jobGuid);
    }
}
