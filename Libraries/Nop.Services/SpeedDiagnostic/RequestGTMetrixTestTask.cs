﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using HarSharp;
using Microsoft.Net.Http.Headers;
using Newtonsoft.Json;
using Nop.Core;
using Nop.Core.Domain.SpeedDiagnostic;
using Nop.Services.Configuration;
using Nop.Services.Logging;
using Nop.Services.ScheduleTasks;

namespace Nop.Services.SpeedDiagnostic
{
    /// <summary>
    /// Represents a task for deleting guest customers
    /// </summary>
    public partial class RequestGTMetrixTestTask : IScheduleTask
    {
        #region Fields

        private readonly ISpeedDiagnosticService _speedDiagnosticService;
        private readonly ILogger _logger;
        private readonly ISettingService _settingService;
        private readonly HttpClient _httpClient;

        #endregion

        #region Ctor

        public RequestGTMetrixTestTask(ISpeedDiagnosticService speedDiagnosticService,
            ILogger logger,
            ISettingService settingService,
            HttpClient httpClient)
        {
            //configure client
            httpClient.BaseAddress = new Uri("https://gtmetrix.com/");
            httpClient.DefaultRequestHeaders.Add(HeaderNames.Accept, MimeTypes.ApplicationJson);

            _speedDiagnosticService = speedDiagnosticService;
            _logger = logger;
            _settingService = settingService;
            _httpClient = httpClient;
        }

        #endregion

        #region Methods

        /// <summary>
        /// Executes a task
        /// </summary>
        public async System.Threading.Tasks.Task ExecuteAsync()
        {
            try
            {
                var apiKey = await _settingService.GetSettingByKeyAsync<string>("SpeedDiagnostic.GTMetrixAPIKey");
                if (string.IsNullOrWhiteSpace(apiKey))
                    await _logger.WarningAsync("Request GTMetrix task failed: Empty API Key.");


                var authString = Convert.ToBase64String(Encoding.UTF8.GetBytes($"{apiKey}:"));
                _httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", authString);

                // Process pending job - Post test request to GTMetrix, then change job status to 'Running'
                var jobs = _speedDiagnosticService.GetAllPendingJobQueue();
                if (jobs != null)
                {
                    var numberOfJobs = 0;
                    var numberOfRequests = await _settingService.GetSettingByKeyAsync<int>("SpeedDiagnostic.GTMetrixNumberOfTestConcurrency"); // number of test concurrency
                    foreach (var job in jobs)
                    {
                        //prepare request parameters
                        var requestParameter = new
                        {
                            data = new
                            {
                                type = "test",
                                attributes = new
                                {
                                    url = job.SiteUrl,
                                    location = job.Location.ToString(),
                                    report = "lighthouse"
                                }
                            }
                        };
                        var requestString = JsonConvert.SerializeObject(requestParameter);
                        var requestContent = new StringContent(requestString, null, "application/vnd.api+json");
                        requestContent.Headers.Remove("charset");
                        requestContent.Headers.Remove("Content-Type");
                        requestContent.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/vnd.api+json");

                        //execute request and get response
                        var httpResponse = await _httpClient.PostAsync("api/2.0/tests", requestContent);

                        //return result
                        var responseString = await httpResponse.Content.ReadAsStringAsync();
                        var result = JsonConvert.DeserializeObject<GTMetrixResult>(responseString);

                        if (!httpResponse.IsSuccessStatusCode)
                        {
                            if ((int)httpResponse.StatusCode == 429)
                            {
                                continue;
                            }

                            if (result.errors != null)
                            {
                                job.Status = "Failed";
                                job.ErrorMessage = $"Fail to generate report - {result.errors.title}";   
                                await _logger.WarningAsync($"Fail to generate report - {result.errors.title}");
                            }
                            else
                            {
                                job.Status = "Failed";
                                job.ErrorMessage = $"Fail to generate report - {result.data.attributes.error}";

                                await _logger.WarningAsync($"Fail to generate report - {result.data.attributes.error}");
                            }
                        }
                        else
                        {
                            job.TestUrl = result.links.self;
                            job.Status = "Running";
                        }

                        await _speedDiagnosticService.UpdateSpeedReportJobQueueAsync(job);

                        //add a product to result
                        numberOfJobs++;
                        if (numberOfJobs >= numberOfRequests)
                            break;
                    }
                }

                // Process running job - Get test request from GTMetrix, then change job status to 'Completed'. (Only for client that save email and leave checking site)
                var runningJobs = _speedDiagnosticService.GetAllRunningJobQueue();
                if (runningJobs != null)
                {
                    var numberOfRunningJobs = 0;
                    var numberOfRequests = await _settingService.GetSettingByKeyAsync<int>("SpeedDiagnostic.GTMetrixNumberOfTestConcurrency"); // number of test concurrency
                    foreach (var job in runningJobs)
                    {
                        var handler = new HttpClientHandler() { AllowAutoRedirect = false };
                        var testHttpClient = new HttpClient(handler);
                        testHttpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", authString);
                        testHttpClient.DefaultRequestHeaders.Add(HeaderNames.Accept, MimeTypes.ApplicationJson);

                        var testUri = job.TestUrl;

                        var jobHttpResponse = await testHttpClient.GetAsync(testUri);

                        //return result
                        var jobResponseString = await jobHttpResponse.Content.ReadAsStringAsync();
                        var gtresult = JsonConvert.DeserializeObject<GTMetrixResult>(jobResponseString);

                        if (gtresult.errors != null || !string.IsNullOrWhiteSpace(gtresult.data.attributes.error))
                        {
                            if (gtresult.errors != null)
                            {
                                job.Status = "Failed";
                                job.ErrorMessage = $"Fail to generate report - {gtresult.errors.title}";
                                await _speedDiagnosticService.UpdateSpeedReportJobQueueAsync(job);
                                continue;
                            }
                            else
                            {
                                job.Status = "Failed";
                                job.ErrorMessage = $"Fail to generate report - {gtresult.data.attributes.error}";
                                await _speedDiagnosticService.UpdateSpeedReportJobQueueAsync(job);
                                continue;
                            }
                        }

                        if ((int)jobHttpResponse.StatusCode == 200)
                        {
                            continue;
                        }

                        // redirect and get report
                        var reportResponse = await _httpClient.GetAsync(gtresult.data.links.report);
                        var reportResponseString = await reportResponse.Content.ReadAsStringAsync();
                        var reportResult = JsonConvert.DeserializeObject<GTMetrixResult>(reportResponseString);

                        if (!reportResponse.IsSuccessStatusCode)
                        {
                            if (reportResult.errors != null)
                            {
                                job.Status = "Failed";
                                job.ErrorMessage = $"Fail to generate report - {reportResult.errors.title}";

                                await _speedDiagnosticService.UpdateSpeedReportJobQueueAsync(job);

                                continue;
                            }
                            else
                            {
                                job.Status = "Failed";
                                job.ErrorMessage = $"Fail to generate report - {reportResult.data.attributes.error}";

                                await _speedDiagnosticService.UpdateSpeedReportJobQueueAsync(job);

                                continue;
                            }
                        }

                        var harResponse = await _httpClient.GetAsync(reportResult.data.links.har);

                        var harString = await harResponse.Content.ReadAsStringAsync();
                        var harResult = HarConvert.Deserialize(harString);

                        var harStringList = new StringBuilder();
                        harStringList.Append("[");
                        foreach (var entry in harResult.Log.Entries)
                        {
                            harStringList.Append($"{{\"url\":\"{WebUtility.UrlEncode(entry.Request.Url.ToString())}\",\"size\":\"{entry.Response.BodySize}\",\"status\":\"{entry.Response.Status}\",\"time\":\"{entry.Time}\"}}");
                            harStringList.Append(",");
                        }
                        harStringList.Append("]");

                        job.Status = "Completed";
                        job.ReportUrl = gtresult.data.links.report;
                        await _speedDiagnosticService.UpdateSpeedReportJobQueueAsync(job);

                        var speedReport = new SpeedReport()
                        {
                            ReportJobQueueId = job.Id,
                            GtMetrixGrade = reportResult.data.attributes.gtmetrix_grade,
                            PerformanceScore = reportResult.data.attributes.performance_score,
                            StructureScore = reportResult.data.attributes.structure_score,
                            HtmlBytes = reportResult.data.attributes.html_bytes,
                            PageBytes = reportResult.data.attributes.page_bytes,
                            PageRequests = reportResult.data.attributes.page_requests,
                            RedirectDuration = reportResult.data.attributes.redirect_duration,
                            ConnectDuration = reportResult.data.attributes.connect_duration,
                            BackendDuration = reportResult.data.attributes.backend_duration,
                            TimeToFirstByte = reportResult.data.attributes.time_to_first_byte,
                            FullyLoadedTime = reportResult.data.attributes.fully_loaded_time,
                            SpeedIndex = reportResult.data.attributes.speed_index,
                            LargestContentfulPaint = reportResult.data.attributes.largest_contentful_paint,
                            TimeToInteractive = reportResult.data.attributes.time_to_interactive,
                            TotalBlockingTime = reportResult.data.attributes.total_blocking_time,
                            CumulativeLayoutShift = (decimal)reportResult.data.attributes.cumulative_layout_shift,
                            Har = harStringList.ToString()
                        };

                        await _speedDiagnosticService.InsertSpeedReportAsync(speedReport);

                        //add a product to result
                        numberOfRunningJobs++;
                        if (numberOfRunningJobs >= numberOfRequests)
                            break;
                    }
                }
            }
            catch (AggregateException exception)
            {
                await _logger.WarningAsync($"Request GTMetrix task failed: {exception}.");
            }
        }

        #endregion

        #region Nested classes
        public class GTMetrixResult
        {
            public Data data { get; set; }
            public Meta meta { get; set; }
            public Links links { get; set; }
            public Errors errors { get; set; }
        }

        public class Data
        {
            public string type { get; set; }
            public string id { get; set; }
            public Attributes attributes { get; set; }
            public Links links { get; set; }
        }

        public class Attributes
        {
            public string source { get; set; }
            public string location { get; set; }
            public string browser { get; set; }
            public string state { get; set; }
            public string created { get; set; }
            public string error { get; set; }
            public string gtmetrix_grade { get; set; }
            public int performance_score { get; set; }
            public int structure_score { get; set; }
            public int html_bytes { get; set; }
            public int page_bytes { get; set; }
            public int page_requests { get; set; }
            public int redirect_duration { get; set; }
            public int connect_duration { get; set; }
            public int backend_duration { get; set; }
            public int time_to_first_byte { get; set; }
            public int fully_loaded_time { get; set; }
            public int speed_index { get; set; }
            public int largest_contentful_paint { get; set; }
            public int time_to_interactive { get; set; }
            public int total_blocking_time { get; set; }
            public float cumulative_layout_shift { get; set; }

        }

        public class Meta
        {
            public decimal credits_left { get; set; }
            public decimal credits_used { get; set; }
        }

        public class Links
        {
            public string self { get; set; }
            public string har { get; set; }
            public string report { get; set; }
        }

        public class Errors
        {
            public string status { get; set; }
            public string code { get; set; }
            public string title { get; set; }
            public string detail { get; set; }
        }

        #endregion

    }
}