﻿using System;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using HarSharp;
using Microsoft.Net.Http.Headers;
using Newtonsoft.Json;
using Nop.Core;
using Nop.Services.Configuration;
using Nop.Services.Logging;
using Nop.Core.Domain.SpeedDiagnostic;
using Nop.Data;
using System.Linq;
using System.Collections.Generic;

namespace Nop.Services.SpeedDiagnostic
{
    /// <summary>
    /// Blog service
    /// </summary>
    public partial class SpeedDiagnosticService : ISpeedDiagnosticService
    {
        #region Fields

        private readonly HttpClient _httpClient;
        private readonly ISettingService _settingService;
        private readonly ILogger _logger;
        private readonly IRepository<SpeedReport> _speedReportRepository;
        private readonly IRepository<SpeedReportJobQueue> _speedReportJobQueueRepository;
        private readonly IWebHelper _webHelper;

        #endregion

        #region Ctor

        public SpeedDiagnosticService(HttpClient httpClient, 
            ISettingService settingService, 
            ILogger logger,
            IRepository<SpeedReport> speedReportRepository,
            IRepository<SpeedReportJobQueue> speedReportJobQueueRepository,
            IWebHelper webHelper)
        {
            //configure client
            httpClient.BaseAddress = new Uri("https://gtmetrix.com/");
            httpClient.DefaultRequestHeaders.Add(HeaderNames.Accept, MimeTypes.ApplicationJson);

            _httpClient = httpClient;
            _settingService = settingService;
            _logger = logger;
            _speedReportRepository = speedReportRepository;
            _speedReportJobQueueRepository = speedReportJobQueueRepository;
            _webHelper = webHelper;
        }

        #endregion

        #region Methods

        public string RetrieveGTMetrixLocation(int locationId)
        {
            switch (locationId)
            {
                case 1:
                    return "Vancouver, Canada";
                case 2:
                    return "London, UK";
                case 3:
                    return "Sydney, Australia";
                case 4:
                    return "San Antonio, TX, USA";
                case 5:
                    return "Mumbai, India";
                case 6:
                    return "São Paulo, Brazil";
                case 7:
                    return "Hong Kong, China";
                default:
                    return "";
            }
        }

        #region Speed report job queue

        public virtual async Task InsertSpeedReportJobQueueAsync(SpeedReportJobQueue job)
        {
            job.JobGuid = Guid.NewGuid();

            await _speedReportJobQueueRepository.InsertAsync(job);
        }

        public virtual async Task<SpeedReportJobQueue> GetJobQueueByIdAsync(int jobId)
        {
            return await _speedReportJobQueueRepository.GetByIdAsync(jobId);
        }

        public virtual IList<SpeedReportJobQueue> GetAllPendingJobQueue()
        {
            var query = _speedReportJobQueueRepository.Table;

            query = query.Where(x => x.Status == "Pending" && x.TestUrl == null);

            return query.OrderBy(x => x.CreatedOnUtc).ToList();
        }

        public virtual IList<SpeedReportJobQueue> GetAllRunningJobQueue()
        {
            var query = _speedReportJobQueueRepository.Table;

            query = query.Where(x => x.Status == "Running" && x.Email != null);

            return query.OrderBy(x => x.CreatedOnUtc).ToList();
        }

        public virtual async Task<IPagedList<SpeedReportJobQueue>> GetAllJobQueueAsync(string siteUrl = null,
            string email = null, int locationId = 0, string status = null, int pageIndex = 0, 
            int pageSize = int.MaxValue, bool showHidden = false)
        {
            return await _speedReportJobQueueRepository.GetAllPagedAsync(query =>
            {
                if (!string.IsNullOrWhiteSpace(siteUrl))
                    query = query.Where(x => x.SiteUrl.Contains(siteUrl));

                if (!string.IsNullOrWhiteSpace(email))
                    query = query.Where(x => x.Email.Contains(email));

                if (locationId > 0)
                    query = query.Where(x => x.Location == locationId);

                if (!string.IsNullOrWhiteSpace(status))
                    query = query.Where(x => x.Status == status);

                query = query.Distinct().OrderByDescending(a => a.Id);

                return query;
            }, pageIndex, pageSize);
        }

        public virtual async Task UpdateSpeedReportJobQueueAsync(SpeedReportJobQueue job)
        {
            await _speedReportJobQueueRepository.UpdateAsync(job);
        }

        public virtual async Task<SpeedReportJobQueue> GetSpeedReportJobQueueByGuidAsync(Guid jobGuid)
        {
            if (jobGuid == Guid.Empty)
                return null;

            var query = from c in _speedReportJobQueueRepository.Table
                        where c.JobGuid == jobGuid
                        orderby c.Id
                        select c;

            return await query.FirstOrDefaultAsync();
        }

        public virtual Task<string> GenerateUrlAsync(SpeedReportJobQueue job)
        {
            if (job == null)
                throw new ArgumentNullException(nameof(job));

            var storeUrl = _webHelper.GetStoreLocation();
            var url = !string.IsNullOrEmpty(job.JobGuid.ToString()) ?
                //use friendly URL
                _webHelper.ModifyQueryString(storeUrl + "wp_speed_result", "jobGuid", job.JobGuid.ToString()) :
                //use ID
                _webHelper.ModifyQueryString(storeUrl + "wp_speed_result", "jobGuid", job.Id.ToString());

            return Task.FromResult(url);
        }

        #endregion

        #region Speed report

        public virtual async Task InsertSpeedReportAsync(SpeedReport report)
        {
            await _speedReportRepository.InsertAsync(report);
        }

        public virtual async Task<SpeedReport> GetSpeedReportByIdAsync(int reportId)
        {
            return await _speedReportRepository.GetByIdAsync(reportId);
        }

        public virtual SpeedReport GetSpeedReportByJobIdAsync(int jobId)
        {
            var query = _speedReportRepository.Table;

            var report = query.FirstOrDefault(x => x.ReportJobQueueId == jobId);

            return report;
        }

        #endregion

        public async Task<(bool success, string report)> GetGTMetrixResultAsync(string jobGuid)
        {
            var job = await GetSpeedReportJobQueueByGuidAsync(new Guid(jobGuid));
            var speedReport = GetSpeedReportByJobIdAsync(job.Id);
            if (speedReport != null)
            {
                return (true, speedReport.Id.ToString());
            }

            try
            {
                var apiKey = await _settingService.GetSettingByKeyAsync<string>("SpeedDiagnostic.GTMetrixAPIKey");
                if (string.IsNullOrWhiteSpace(apiKey))
                    return (false, "Empty API Key.");

                var authString = Convert.ToBase64String(Encoding.UTF8.GetBytes($"{apiKey}:"));

                var handler = new HttpClientHandler() { AllowAutoRedirect = false };
                var testHttpClient = new HttpClient(handler);
                testHttpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", authString);
                testHttpClient.DefaultRequestHeaders.Add(HeaderNames.Accept, MimeTypes.ApplicationJson);

                //execute request and get response
                if (string.IsNullOrWhiteSpace(job.TestUrl))
                {
                    job.Status = "Failed";
                    job.ErrorMessage = "Request took too long to process, please try again later.";
                    await UpdateSpeedReportJobQueueAsync(job);

                    return (false, "Request took too long to process, please try again later.");
                }
                var testUri = job.TestUrl;

                var httpResponse = await testHttpClient.GetAsync(testUri);

                //return result
                var responseString = await httpResponse.Content.ReadAsStringAsync();
                var result = JsonConvert.DeserializeObject<GTMetrixResult>(responseString);

                if (result.errors != null || !string.IsNullOrWhiteSpace(result.data.attributes.error))
                {
                    if (result.errors != null)
                    {
                        job.Status = "Failed";
                        job.ErrorMessage = $"Fail to generate report - {result.errors.title}";
                        await UpdateSpeedReportJobQueueAsync(job);

                        return (false, $"Fail to generate report - {result.errors.title}");
                    }
                    else
                    {
                        job.Status = "Failed";
                        job.ErrorMessage = $"Fail to generate report - {result.data.attributes.error}";
                        await UpdateSpeedReportJobQueueAsync(job);

                        return (false, $"Fail to generate report - {result.data.attributes.error}");
                    }
                }

                if ((int)httpResponse.StatusCode == 200)
                {
                    job.Status = "Failed";
                    job.ErrorMessage = "Request took too long to process, please try again later.";
                    await UpdateSpeedReportJobQueueAsync(job);

                    return (false, "Request took too long to process, please try again later.");
                }

                // redirect and get report
                _httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", authString);

                var reportResponse = await _httpClient.GetAsync(result.data.links.report);
                var reportResponseString = await reportResponse.Content.ReadAsStringAsync();
                var reportResult = JsonConvert.DeserializeObject<GTMetrixResult>(reportResponseString);

                if (!reportResponse.IsSuccessStatusCode)
                {
                    if (reportResult.errors != null)
                    {
                        job.Status = "Failed";
                        job.ErrorMessage = $"Fail to generate report - {reportResult.errors.title}";

                        await UpdateSpeedReportJobQueueAsync(job);

                        return (false, $"Fail to generate report - {reportResult.errors.title}");
                    }
                    else
                    {
                        job.Status = "Failed";
                        job.ErrorMessage = $"Fail to generate report - {reportResult.data.attributes.error}";

                        await UpdateSpeedReportJobQueueAsync(job);

                        return (false, $"Fail to generate report - {reportResult.data.attributes.error}");
                    }
                }

                var harResponse = await _httpClient.GetAsync(reportResult.data.links.har);

                var harString = await harResponse.Content.ReadAsStringAsync(); 
                var harResult = HarConvert.Deserialize(harString);

                var harStringList = new StringBuilder();
                harStringList.Append("[");
                foreach (var entry in harResult.Log.Entries)
                {
                    harStringList.Append($"{{\"url\":\"{WebUtility.UrlEncode(entry.Request.Url.ToString())}\",\"size\":\"{entry.Response.BodySize}\",\"status\":\"{entry.Response.Status}\",\"time\":\"{entry.Time}\"}}");
                    harStringList.Append(",");
                }
                harStringList.Append("]");

                job.Status = "Completed";
                job.ReportUrl = result.data.links.report;
                await UpdateSpeedReportJobQueueAsync(job);

                speedReport = new SpeedReport()
                {
                    ReportJobQueueId = job.Id,
                    GtMetrixGrade = reportResult.data.attributes.gtmetrix_grade,
                    PerformanceScore = reportResult.data.attributes.performance_score,
                    StructureScore = reportResult.data.attributes.structure_score,
                    HtmlBytes = reportResult.data.attributes.html_bytes,
                    PageBytes = reportResult.data.attributes.page_bytes,
                    PageRequests = reportResult.data.attributes.page_requests,
                    RedirectDuration = reportResult.data.attributes.redirect_duration,
                    ConnectDuration = reportResult.data.attributes.connect_duration,
                    BackendDuration = reportResult.data.attributes.backend_duration,
                    TimeToFirstByte = reportResult.data.attributes.time_to_first_byte,
                    FullyLoadedTime = reportResult.data.attributes.fully_loaded_time,
                    SpeedIndex = reportResult.data.attributes.speed_index,
                    LargestContentfulPaint = reportResult.data.attributes.largest_contentful_paint,
                    TimeToInteractive = reportResult.data.attributes.time_to_interactive,
                    TotalBlockingTime = reportResult.data.attributes.total_blocking_time,
                    CumulativeLayoutShift = (decimal)reportResult.data.attributes.cumulative_layout_shift,
                    Har = harStringList.ToString()
                };

                await InsertSpeedReportAsync(speedReport);

                return (true, speedReport.Id.ToString());
            }
            catch (AggregateException exception)
            {
                if (job != null)
                {
                    job.Status = "Failed";
                    job.ErrorMessage = $"Fail to generate report - {exception}";

                    await UpdateSpeedReportJobQueueAsync(job);
                }

                return (false, $"Fail to generate report - {exception}");
            }
        }

        public async Task<int> CheckResultStatusAsync(string jobGuid) // return 1 = GTMetrix finished processing, 2 = GTMetrix still processing, 3 =  GTMetrix failed in processing the test, 4 = GTMetrix failed in requesting the test 
        {
            var apiKey = await _settingService.GetSettingByKeyAsync<string>("SpeedDiagnostic.GTMetrixAPIKey");
            if (string.IsNullOrWhiteSpace(apiKey))
                return 3;

            var authString = Convert.ToBase64String(Encoding.UTF8.GetBytes($"{apiKey}:"));

            var handler = new HttpClientHandler() { AllowAutoRedirect = false };
            var testHttpClient = new HttpClient(handler);
            testHttpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", authString);
            testHttpClient.DefaultRequestHeaders.Add(HeaderNames.Accept, MimeTypes.ApplicationJson);

            //execute request and get response
            var job = await GetSpeedReportJobQueueByGuidAsync(new Guid(jobGuid));
            if (job.Status == "Completed")
            {
                return 1;
            }

            if (!string.IsNullOrWhiteSpace(job.ErrorMessage))
            {
                return 4;
            }

            if (string.IsNullOrWhiteSpace(job.TestUrl))
            {
                return 2;
            }
            var testUri = job.TestUrl;
            var httpResponse = await testHttpClient.GetAsync(testUri);

            if ((int)httpResponse.StatusCode == 303)
            {
                return 1;
            } 
            else if ((int)httpResponse.StatusCode == 200)
            {
                return 2;
            }
            else
            {
                return 3;
            }
        }

        #endregion

        #region Nested classes
        public class GTMetrixResult
        {
            public Data data { get; set; }
            public Meta meta { get; set; }
            public Links links { get; set; }
            public Errors errors { get; set; }
        }

        public class Data
        {
            public string type { get; set; }
            public string id { get; set; }
            public Attributes attributes { get; set; }
            public Links links { get; set; }
        }

        public class Attributes
        {
            public string source { get; set; }
            public string location { get; set; }
            public string browser { get; set; }
            public string state { get; set; }
            public string created { get; set; }
            public string error { get; set; }
            public string gtmetrix_grade { get; set; }
            public int performance_score { get; set; }
            public int structure_score { get; set; }
            public int html_bytes { get; set; }
            public int page_bytes { get; set; }
            public int page_requests { get; set; }
            public int redirect_duration { get; set; }
            public int connect_duration { get; set; }
            public int backend_duration { get; set; }
            public int time_to_first_byte { get; set; }
            public int fully_loaded_time { get; set; }
            public int speed_index { get; set; }
            public int largest_contentful_paint { get; set; }
            public int time_to_interactive { get; set; }
            public int total_blocking_time { get; set; }
            public float cumulative_layout_shift { get; set; }

        }

        public class Meta
        {
            public decimal credits_left { get; set; }
            public decimal credits_used { get; set;}
        }

        public class Links
        {
            public string self { get; set; }
            public string har { get; set; }
            public string report { get; set; }
        }

        public class Errors
        {
            public string status { get; set; }
            public string code { get; set; }
            public string title { get; set; }
            public string detail { get; set; }
        }

        #endregion
    }
}