﻿using System;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Nop.Core.Domain.SpeedDiagnostic;
using Nop.Core.Domain.Common;
using Nop.Services.SpeedDiagnostic;
using Nop.Services.Common;
using Nop.Services.Localization;
using Nop.Services.Logging;
using Nop.Services.Messages;
using Nop.Services.Security;
using Nop.Web.Areas.Admin.Factories;
using Nop.Web.Areas.Admin.Infrastructure.Mapper.Extensions;
using Nop.Web.Areas.Admin.Models.Customers;
using Nop.Web.Framework.Controllers;
using Nop.Web.Framework.Mvc.Filters;

namespace Nop.Web.Areas.Admin.Controllers
{
    public partial class SpeedDiagnosticController : BaseAdminController
    {
        #region Fields

        private readonly IAddressService _addressService;
        private readonly ISpeedDiagnosticModelFactory _speedDiagnosticModelFactory;
        private readonly ISpeedDiagnosticService _speedDiagnosticService;
        private readonly ICustomerActivityService _customerActivityService;
        private readonly ILocalizationService _localizationService;
        private readonly INotificationService _notificationService;
        private readonly IPermissionService _permissionService;

        #endregion

        #region Ctor

        public SpeedDiagnosticController(IAddressService addressService,
            ISpeedDiagnosticModelFactory speedDiagnosticModelFactory,
            ISpeedDiagnosticService speedDiagnosticService,
            ICustomerActivityService customerActivityService,
            ILocalizationService localizationService,
            INotificationService notificationService,
            IPermissionService permissionService)
        {
            _addressService = addressService;
            _speedDiagnosticModelFactory = speedDiagnosticModelFactory;
            _speedDiagnosticService = speedDiagnosticService;
            _customerActivityService = customerActivityService;
            _localizationService = localizationService;
            _notificationService = notificationService;
            _permissionService = permissionService;
        }

        #endregion

        #region Methods

        public virtual IActionResult Index()
        {
            return RedirectToAction("List");
        }

        public virtual async Task<IActionResult> List()
        {
            if (!await _permissionService.AuthorizeAsync(StandardPermissionProvider.ManageCustomers))
                return AccessDeniedView();

            //prepare model
            var model = await _speedDiagnosticModelFactory.PrepareSpeedDiagnosticSearchModelAsync(new SpeedDiagnosticSearchModel());

            return View(model);
        }

        [HttpPost]
        public virtual async Task<IActionResult> List(SpeedDiagnosticSearchModel searchModel)
        {
            if (!await _permissionService.AuthorizeAsync(StandardPermissionProvider.ManageCustomers))
                return await AccessDeniedDataTablesJson();

            //prepare model
            var model = await _speedDiagnosticModelFactory.PrepareSpeedDiagnosticListModelAsync(searchModel);

            return Json(model);
        }

        public virtual async Task<IActionResult> Edit(int id)
        {
            if (!await _permissionService.AuthorizeAsync(StandardPermissionProvider.ManageCustomers))
                return AccessDeniedView();

            var job = await _speedDiagnosticService.GetJobQueueByIdAsync(id);
            if (job == null)
                return RedirectToAction("List");

            //prepare model
            var model = await _speedDiagnosticModelFactory.PrepareSpeedDiagnosticModelAsync(new SpeedDiagnosticModel(), job);

            return View(model);
        }

        [HttpPost]
        public virtual async Task<IActionResult> HarList(SpeedDiagnosticHarSearchModel searchModel)
        {
            if (!await _permissionService.AuthorizeAsync(StandardPermissionProvider.ManageCustomers))
                return await AccessDeniedDataTablesJson();

            var report = await _speedDiagnosticService.GetSpeedReportByIdAsync(searchModel.ReportId)
                ?? throw new ArgumentException("No customer found with the specified id");

            //prepare model
            var model = await _speedDiagnosticModelFactory.PrepareHarListModelAsync(searchModel, report);

            return Json(model);
        }

        #endregion
    }
}