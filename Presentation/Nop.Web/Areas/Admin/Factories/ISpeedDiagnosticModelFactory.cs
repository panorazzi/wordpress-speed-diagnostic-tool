﻿using System.Threading.Tasks;
using Nop.Core.Domain.SpeedDiagnostic;
using Nop.Web.Areas.Admin.Models.Customers;

namespace Nop.Web.Areas.Admin.Factories
{
    public partial interface ISpeedDiagnosticModelFactory
    {
        Task<SpeedDiagnosticSearchModel> PrepareSpeedDiagnosticSearchModelAsync(SpeedDiagnosticSearchModel searchModel);

        Task<SpeedDiagnosticListModel> PrepareSpeedDiagnosticListModelAsync(SpeedDiagnosticSearchModel searchModel);

        Task<SpeedDiagnosticModel> PrepareSpeedDiagnosticModelAsync(SpeedDiagnosticModel model, SpeedReportJobQueue job, bool excludeProperties = false);

        Task<SpeedDiagnosticHarListModel> PrepareHarListModelAsync(SpeedDiagnosticHarSearchModel searchModel, SpeedReport report);
    }
}