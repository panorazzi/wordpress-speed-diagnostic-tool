﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc.Rendering;
using Nop.Core.Domain.SpeedDiagnostic;
using Nop.Services.SpeedDiagnostic;
using Nop.Services.Catalog;
using Nop.Services.Common;
using Nop.Services.Customers;
using Nop.Services.Directory;
using Nop.Services.Helpers;
using Nop.Services.Localization;
using Nop.Services.Orders;
using Nop.Web.Areas.Admin.Infrastructure.Mapper.Extensions;
using Nop.Web.Areas.Admin.Models.Customers;
using Nop.Web.Framework.Models.Extensions;
using Newtonsoft.Json;


namespace Nop.Web.Areas.Admin.Factories
{
    /// <summary>
    /// Represents the speed diagnostic model factory implementation
    /// </summary>
    public partial class SpeedDiagnosticModelFactory : ISpeedDiagnosticModelFactory
    {
        #region Fields

        private readonly IAddressModelFactory _addressModelFactory;
        private readonly IAddressService _addressService;
        private readonly ISpeedDiagnosticService _speedDiagnosticService;
        private readonly IBaseAdminModelFactory _baseAdminModelFactory;
        private readonly ICountryService _countryService;
        private readonly ICustomerService _customerService;
        private readonly IDateTimeHelper _dateTimeHelper;
        private readonly ILocalizationService _localizationService;
        private readonly IOrderService _orderService;
        private readonly IPriceFormatter _priceFormatter;
        private readonly IStateProvinceService _stateProvinceService;

        #endregion

        #region Ctor

        public SpeedDiagnosticModelFactory(IAddressModelFactory addressModelFactory,
            IAddressService addressService,
            ISpeedDiagnosticService speedDiagnosticService,
            IBaseAdminModelFactory baseAdminModelFactory,
            ICountryService countryService,
            ICustomerService customerService,
            IDateTimeHelper dateTimeHelper,
            ILocalizationService localizationService,
            IOrderService orderService,
            IPriceFormatter priceFormatter,
            IStateProvinceService stateProvinceService)
        {
            _addressModelFactory = addressModelFactory;
            _addressService = addressService;
            _speedDiagnosticService = speedDiagnosticService;
            _baseAdminModelFactory = baseAdminModelFactory;
            _countryService = countryService;
            _customerService = customerService;
            _dateTimeHelper = dateTimeHelper;
            _localizationService = localizationService;
            _orderService = orderService;
            _priceFormatter = priceFormatter;
            _stateProvinceService = stateProvinceService;
        }

        #endregion

        #region Utilities

        private IList<SelectListItem> PrepareLocationModel()
        {
            var model = new List<SelectListItem>();

            // 7 location for now for free plan user, add more in future
            model.Add(new SelectListItem
            {
                Text = "Vancouver, Canada (Default Location)",
                Value = "1"
            });
            model.Add(new SelectListItem
            {
                Text = "London, UK",
                Value = "2"
            });
            model.Add(new SelectListItem
            {
                Text = "Sydney, Australia",
                Value = "3"
            });
            model.Add(new SelectListItem
            {
                Text = "San Antonio, TX, USA",
                Value = "4"
            });
            model.Add(new SelectListItem
            {
                Text = "Mumbai, India",
                Value = "5"
            });
            model.Add(new SelectListItem
            {
                Text = "São Paulo, Brazil",
                Value = "6"
            });
            model.Add(new SelectListItem
            {
                Text = "Hong Kong, China",
                Value = "7"
            });
            model.Insert(0, new SelectListItem
            {
                Text = "All",
                Value = "0"
            });

            return model;
        }

        private string RetrieveReportStatus(int statusId)
        {
            switch (statusId)
            {
                case 1:
                    return "Pending";
                case 2:
                    return "Running";
                case 3:
                    return "Completed";
                case 4:
                    return "Failed";
                default:
                    return "";
            }
        }


        #endregion

        #region Methods

        public virtual Task<SpeedDiagnosticSearchModel> PrepareSpeedDiagnosticSearchModelAsync(SpeedDiagnosticSearchModel searchModel)
        {
            if (searchModel == null)
                throw new ArgumentNullException(nameof(searchModel));

            searchModel.AvailableLocation = PrepareLocationModel();

            var statusList = new List<SelectListItem>();
            statusList.Add(new SelectListItem
            {
                Text = "Pending",
                Value = "1"
            });
            statusList.Add(new SelectListItem
            {
                Text = "Running",
                Value = "2"
            });
            statusList.Add(new SelectListItem
            {
                Text = "Completed",
                Value = "3"
            });
            statusList.Add(new SelectListItem
            {
                Text = "Failed",
                Value = "4"
            });
            statusList.Insert(0, new SelectListItem
            {
                Text = "All",
                Value = "0"
            });
            searchModel.AvailableStatus = statusList;
            searchModel.StatusId = 3;

            //prepare page parameters
            searchModel.SetGridPageSize();

            return Task.FromResult(searchModel);
        }

        public virtual async Task<SpeedDiagnosticListModel> PrepareSpeedDiagnosticListModelAsync(SpeedDiagnosticSearchModel searchModel)
        {
            if (searchModel == null)
                throw new ArgumentNullException(nameof(searchModel));

            var status = RetrieveReportStatus(searchModel.StatusId);
            var jobs = await _speedDiagnosticService.GetAllJobQueueAsync(searchModel.SiteUrl,
                searchModel.Email,
                searchModel.LocationId,
                status,
                searchModel.Page - 1, searchModel.PageSize, true);

            //prepare list model
            var model = await new SpeedDiagnosticListModel().PrepareToGridAsync(searchModel, jobs, () =>
            {
                //fill in model values from the entity
                return jobs.SelectAwait(async job =>
                {
                    var jobModel = new SpeedDiagnosticModel()
                    {
                        Id = job.Id,
                        SiteUrl = job.SiteUrl,
                        Email = job.Email,
                        Location = _speedDiagnosticService.RetrieveGTMetrixLocation(job.Location),
                        Status = job.Status,
                        CreatedOnUtc = await _dateTimeHelper.ConvertToUserTimeAsync(job.CreatedOnUtc, DateTimeKind.Utc),
                    };

                    var reportModel = new SpeedDiagnosticModel.SpeedReportModel()
                    {
                        GtMetrixGrade = "",
                        PerformanceScore = 0,
                    };

                    if (job.Status == "Completed")
                    {
                        var report = _speedDiagnosticService.GetSpeedReportByJobIdAsync(job.Id);
                        if (report != null)
                        {
                            reportModel.GtMetrixGrade = report.GtMetrixGrade;
                            reportModel.PerformanceScore = report.PerformanceScore;
                        }
                    }

                    jobModel.Report = reportModel;

                    return jobModel;
                });
            });

            return model;
        }

        public virtual async Task<SpeedDiagnosticModel> PrepareSpeedDiagnosticModelAsync(SpeedDiagnosticModel model, SpeedReportJobQueue job, bool excludeProperties = false)
        {
            //fill in model values from the entity
            if (job != null)
            {
                model.SiteUrl = job.SiteUrl;
                model.Email = job.Email;
                model.Location = _speedDiagnosticService.RetrieveGTMetrixLocation(job.Location);
                model.Status = job.Status;
                model.GTMetrixReportUrl = job.ReportUrl;
                model.GTMetrixTestUrl = job.TestUrl;
                model.Id = job.Id;
                model.JobGuid = job.JobGuid;
                model.ErrorMessage = job.ErrorMessage;
                model.CreatedOnUtc = await _dateTimeHelper.ConvertToUserTimeAsync(job.CreatedOnUtc, DateTimeKind.Utc);
                if (job.Status == "Completed")
                {
                    model.ReportUrl = await _speedDiagnosticService.GenerateUrlAsync(job);
                    var report = _speedDiagnosticService.GetSpeedReportByJobIdAsync(job.Id);
                    if (report != null)
                    {
                        model.ReportId = report.Id;
                        var reportModel = new SpeedDiagnosticModel.SpeedReportModel()
                        {
                            GtMetrixGrade = report.GtMetrixGrade,
                            PerformanceScore = report.PerformanceScore,
                            StructureScore = report.StructureScore,
                            HtmlBytes = report.HtmlBytes,
                            PageBytes = report.PageBytes,
                            PageRequests = report.PageRequests,
                            RedirectDuration = report.RedirectDuration,
                            ConnectDuration = report.ConnectDuration,
                            BackendDuration = report.BackendDuration,
                            TimeToFirstByte = report.TimeToFirstByte,
                            FullyLoadedTime = report.FullyLoadedTime,
                            SpeedIndex = report.SpeedIndex,
                            LargestContentfulPaint = report.LargestContentfulPaint,
                            TimeToInteractive = report.TimeToInteractive,
                            TotalBlockingTime = report.TotalBlockingTime,
                            CumulativeLayoutShift = report.CumulativeLayoutShift,
                        };

                        model.Report = reportModel;

                    }
                }
            }

            return model;
        }

        public virtual Task<SpeedDiagnosticHarListModel> PrepareHarListModelAsync(SpeedDiagnosticHarSearchModel searchModel, SpeedReport report)
        {
            if (searchModel == null)
                throw new ArgumentNullException(nameof(searchModel));

            if (report == null)
                throw new ArgumentNullException(nameof(report));

            var allHars = JsonConvert.DeserializeObject<IList<Har>>(report.Har);
            var hars = allHars.ToPagedList(searchModel);

            //prepare list model
            var model = new SpeedDiagnosticHarListModel().PrepareToGrid(searchModel, hars, () =>
            {
                //fill in model values from the entity
                return hars.Select(har =>
                {
                    var harModel = new SpeedDiagnosticHarModel()
                    {
                        Url = System.Net.WebUtility.UrlDecode(har.Url),
                        Size = har.Size,
                        Status = har.Status,
                        Time = har.Time,
                    };

                    return harModel;
                });
            });

            return Task.FromResult(model);
        }

        #endregion

        public record Har
        {
            public string Url { get; set; }
            public string Size { get; set; }
            public string Status { get; set; }
            public string Time { get; set; }

        }
    }
}