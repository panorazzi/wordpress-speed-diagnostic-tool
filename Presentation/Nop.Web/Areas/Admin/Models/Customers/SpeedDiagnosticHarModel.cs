﻿using System;
using System.ComponentModel.DataAnnotations;
using Nop.Web.Framework.Models;
using System.Collections.Generic;

namespace Nop.Web.Areas.Admin.Models.Customers
{
    /// <summary>
    /// Represents a report model
    /// </summary>
    public partial record SpeedDiagnosticHarModel : BaseNopEntityModel
    {
        public string Url { get; set; }
        public string Size { get; set; }
        public string Status { get; set; }
        public string Time { get; set; }
    }
}