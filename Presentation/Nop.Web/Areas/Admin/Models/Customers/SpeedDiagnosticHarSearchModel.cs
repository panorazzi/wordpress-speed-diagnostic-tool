﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Mvc.Rendering;
using Nop.Web.Framework.Models;

namespace Nop.Web.Areas.Admin.Models.Customers
{
    /// <summary>
    /// Represents a speed diagnostic report search model
    /// </summary>
    public partial record SpeedDiagnosticHarSearchModel : BaseSearchModel
    {
        #region Properties

        public int ReportId { get; set; }

        #endregion
    }
}