﻿using System;
using System.ComponentModel.DataAnnotations;
using Nop.Web.Framework.Models;
using System.Collections.Generic;

namespace Nop.Web.Areas.Admin.Models.Customers
{
    /// <summary>
    /// Represents a report model
    /// </summary>
    public partial record SpeedDiagnosticModel : BaseNopEntityModel
    {
        public SpeedDiagnosticModel()
        {
            JobGuid = Guid.NewGuid();
            SpeedDiagnosticHarSearchModel = new SpeedDiagnosticHarSearchModel();
        }

        public string ReportUrl { get; set; }

        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }

        public Guid JobGuid { get; set; }

        public string SiteUrl { get; set; }

        public string Location { get; set; }

        public string Status { get; set; }

        public string GTMetrixTestUrl { get; set; }

        public string GTMetrixReportUrl { get; set; }

        public string ErrorMessage { get; set; }

        public DateTime CreatedOnUtc { get; set; }

        public SpeedReportModel Report { get; set; }

        public int ReportId { get; set; }

        public SpeedDiagnosticHarSearchModel SpeedDiagnosticHarSearchModel { get; set;}

        public record SpeedReportModel
        {
            public string GtMetrixGrade { get; set; }

            public int PerformanceScore { get; set; }

            public int StructureScore { get; set; }

            public int HtmlBytes { get; set; }

            public int PageBytes { get; set; }

            public int PageRequests { get; set; }

            public int RedirectDuration { get; set; }

            public int ConnectDuration { get; set; }

            public int BackendDuration { get; set; }

            public int TimeToFirstByte { get; set; }

            public int FullyLoadedTime { get; set; }

            public int SpeedIndex { get; set; }

            public int LargestContentfulPaint { get; set; }

            public int TimeToInteractive { get; set; }

            public int TotalBlockingTime { get; set; }

            public decimal CumulativeLayoutShift { get; set; }

        }
    }
}