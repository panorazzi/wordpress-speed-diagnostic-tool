﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Mvc.Rendering;
using Nop.Web.Framework.Models;

namespace Nop.Web.Areas.Admin.Models.Customers
{
    /// <summary>
    /// Represents a speed diagnostic report search model
    /// </summary>
    public partial record SpeedDiagnosticSearchModel : BaseSearchModel
    {
        public SpeedDiagnosticSearchModel()
        {
            AvailableLocation = new List<SelectListItem>();
            AvailableStatus = new List<SelectListItem>();
        }

        #region Properties

        public string SiteUrl { get; set; }

        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }

        public int LocationId { get; set; }
        public IList<SelectListItem> AvailableLocation { get; set; }

        public int StatusId { get; set; }
        public IList<SelectListItem> AvailableStatus { get; set; }

        #endregion
    }
}