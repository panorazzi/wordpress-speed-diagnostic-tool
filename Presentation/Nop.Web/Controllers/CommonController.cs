﻿/* Decompiled with ILSpy */

using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;
using Nop.Core;
using Nop.Core.Domain;
using Nop.Core.Domain.Common;
using Nop.Core.Domain.Customers;
using Nop.Core.Domain.Directory;
using Nop.Core.Domain.Localization;
using Nop.Core.Domain.Security;
using Nop.Core.Domain.SpeedDiagnostic;
using Nop.Core.Domain.Stores;
using Nop.Core.Domain.Tax;
using Nop.Core.Domain.Vendors;
using Nop.Core.Infrastructure;
using Nop.Services.Common;
using Nop.Services.Directory;
using Nop.Services.Html;
using Nop.Services.Localization;
using Nop.Services.Logging;
using Nop.Services.Messages;
using Nop.Services.SpeedDiagnostic;
using Nop.Services.Vendors;
using Nop.Web.Factories;
using Nop.Web.Framework.Mvc.Filters;
using Nop.Web.Framework.Themes;
using Nop.Web.Models.Common;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace Nop.Web.Controllers
{
    [AutoValidateAntiforgeryToken]
    public class CommonController : BasePublicController
    {
        private readonly CaptchaSettings _captchaSettings;

        private readonly CommonSettings _commonSettings;

        private readonly ICommonModelFactory _commonModelFactory;

        private readonly ICurrencyService _currencyService;

        private readonly ICustomerActivityService _customerActivityService;

        private readonly IGenericAttributeService _genericAttributeService;

        private readonly IHtmlFormatter _htmlFormatter;

        private readonly ILanguageService _languageService;

        private readonly ILocalizationService _localizationService;

        private readonly IStoreContext _storeContext;

        private readonly IThemeContext _themeContext;

        private readonly IVendorService _vendorService;

        private readonly IWorkContext _workContext;

        private readonly IWorkflowMessageService _workflowMessageService;

        private readonly LocalizationSettings _localizationSettings;

        private readonly SitemapSettings _sitemapSettings;

        private readonly SitemapXmlSettings _sitemapXmlSettings;

        private readonly StoreInformationSettings _storeInformationSettings;

        private readonly VendorSettings _vendorSettings;

        public CommonController(CaptchaSettings captchaSettings, CommonSettings commonSettings, ICommonModelFactory commonModelFactory, ICurrencyService currencyService, ICustomerActivityService customerActivityService, IGenericAttributeService genericAttributeService, IHtmlFormatter htmlFormatter, ILanguageService languageService, ILocalizationService localizationService, IStoreContext storeContext, IThemeContext themeContext, IVendorService vendorService, IWorkContext workContext, IWorkflowMessageService workflowMessageService, LocalizationSettings localizationSettings, SitemapSettings sitemapSettings, SitemapXmlSettings sitemapXmlSettings, StoreInformationSettings storeInformationSettings, VendorSettings vendorSettings)
        {
            _captchaSettings = captchaSettings;
            _commonSettings = commonSettings;
            _commonModelFactory = commonModelFactory;
            _currencyService = currencyService;
            _customerActivityService = customerActivityService;
            _genericAttributeService = genericAttributeService;
            _htmlFormatter = htmlFormatter;
            _languageService = languageService;
            _localizationService = localizationService;
            _storeContext = storeContext;
            _themeContext = themeContext;
            _vendorService = vendorService;
            _workContext = workContext;
            _workflowMessageService = workflowMessageService;
            _localizationSettings = localizationSettings;
            _sitemapSettings = sitemapSettings;
            _sitemapXmlSettings = sitemapXmlSettings;
            _storeInformationSettings = storeInformationSettings;
            _vendorSettings = vendorSettings;
        }

        public virtual IActionResult PageNotFound()
        {
            ((ControllerBase)(object)this).Response.StatusCode = 404;
            ((ControllerBase)(object)this).Response.ContentType = "text/html";
            return ((Controller)(object)this).View();
        }

        [CheckAccessClosedStore(true)]
        [CheckAccessPublicStore(true)]
        public virtual async Task<IActionResult> SetLanguage(int langid, string returnUrl = "")
        {
            Language language = await _languageService.GetLanguageByIdAsync(langid);
            Language obj = language;
            if (obj != null && !obj.Published)
            {
                language = await _workContext.GetWorkingLanguageAsync();
            }
            if (string.IsNullOrEmpty(returnUrl))
            {
                returnUrl = ((ControllerBase)(object)this).Url.RouteUrl("Homepage");
            }
            if (_localizationSettings.SeoFriendlyUrlsForLanguagesEnabled)
            {
                if ((await returnUrl.IsLocalizedUrlAsync(((ControllerBase)(object)this).Request.PathBase, isRawPath: true)).Item1)
                {
                    returnUrl = returnUrl.RemoveLanguageSeoCodeFromUrl(((ControllerBase)(object)this).Request.PathBase, isRawPath: true);
                }
                returnUrl = returnUrl.AddLanguageSeoCodeToUrl(((ControllerBase)(object)this).Request.PathBase, isRawPath: true, language);
            }
            await _workContext.SetWorkingLanguageAsync(language);
            if (!((ControllerBase)(object)this).Url.IsLocalUrl(returnUrl))
            {
                returnUrl = ((ControllerBase)(object)this).Url.RouteUrl("Homepage");
            }
            return ((ControllerBase)(object)this).Redirect(returnUrl);
        }

        [CheckAccessPublicStore(true)]
        public virtual async Task<IActionResult> SetCurrency(int customerCurrency, string returnUrl = "")
        {
            Currency currency = await _currencyService.GetCurrencyByIdAsync(customerCurrency);
            if (currency != null)
            {
                await _workContext.SetWorkingCurrencyAsync(currency);
            }
            if (string.IsNullOrEmpty(returnUrl))
            {
                returnUrl = ((ControllerBase)(object)this).Url.RouteUrl("Homepage");
            }
            if (!((ControllerBase)(object)this).Url.IsLocalUrl(returnUrl))
            {
                returnUrl = ((ControllerBase)(object)this).Url.RouteUrl("Homepage");
            }
            return ((ControllerBase)(object)this).Redirect(returnUrl);
        }

        [CheckAccessPublicStore(true)]
        public virtual async Task<IActionResult> SetTaxType(int customerTaxType, string returnUrl = "")
        {
            TaxDisplayType taxDisplayType = (TaxDisplayType)Enum.ToObject(typeof(TaxDisplayType), customerTaxType);
            await _workContext.SetTaxDisplayTypeAsync(taxDisplayType);
            if (string.IsNullOrEmpty(returnUrl))
            {
                returnUrl = ((ControllerBase)(object)this).Url.RouteUrl("Homepage");
            }
            if (!((ControllerBase)(object)this).Url.IsLocalUrl(returnUrl))
            {
                returnUrl = ((ControllerBase)(object)this).Url.RouteUrl("Homepage");
            }
            return ((ControllerBase)(object)this).Redirect(returnUrl);
        }

        [CheckAccessClosedStore(true)]
        public virtual async Task<IActionResult> ContactUs()
        {
            ContactUsModel model = new ContactUsModel();
            return ((Controller)(object)this).View((object?)(await _commonModelFactory.PrepareContactUsModelAsync(model, excludeProperties: false)));
        }

        [HttpPost]
        [ActionName("ContactUs")]
        [ValidateCaptcha("captchaValid")]
        [CheckAccessClosedStore(true)]
        public virtual async Task<IActionResult> ContactUsSend(ContactUsModel model, bool captchaValid)
        {
            if (_captchaSettings.Enabled && _captchaSettings.ShowOnContactUsPage && !captchaValid)
            {
                ModelStateDictionary modelState = ((ControllerBase)(object)this).ModelState;
                modelState.AddModelError("", await _localizationService.GetResourceAsync("Common.WrongCaptchaMessage"));
            }
            model = await _commonModelFactory.PrepareContactUsModelAsync(model, excludeProperties: true);
            if (((ControllerBase)(object)this).ModelState.IsValid)
            {
                string subject = (_commonSettings.SubjectFieldOnContactUsForm ? model.Subject : null);
                string body = _htmlFormatter.FormatText(model.Enquiry, stripTags: false, convertPlainTextToHtml: true, allowHtml: false, allowBbCode: false, resolveLinks: false, addNoFollowTag: false);
                IWorkflowMessageService workflowMessageService = _workflowMessageService;
                await workflowMessageService.SendContactUsMessageAsync(((BaseEntity)(await _workContext.GetWorkingLanguageAsync())).Id, model.Email.Trim(), model.FullName, subject, body);
                model.SuccessfullySent = true;
                ContactUsModel contactUsModel = model;
                contactUsModel.Result = await _localizationService.GetResourceAsync("ContactUs.YourEnquiryHasBeenSent");
                ICustomerActivityService customerActivityService = _customerActivityService;
                await customerActivityService.InsertActivityAsync("PublicStore.ContactUs", await _localizationService.GetResourceAsync("ActivityLog.PublicStore.ContactUs"));
                return ((Controller)(object)this).View((object?)model);
            }
            return ((Controller)(object)this).View((object?)model);
        }

        public virtual async Task<IActionResult> ContactVendor(int vendorId)
        {
            if (!_vendorSettings.AllowCustomersToContactVendors)
            {
                return ((ControllerBase)(object)this).RedirectToRoute("Homepage");
            }
            Vendor vendor = await _vendorService.GetVendorByIdAsync(vendorId);
            if (vendor == null || !vendor.Active || vendor.Deleted)
            {
                return ((ControllerBase)(object)this).RedirectToRoute("Homepage");
            }
            ContactVendorModel model = new ContactVendorModel();
            return ((Controller)(object)this).View((object?)(await _commonModelFactory.PrepareContactVendorModelAsync(model, vendor, excludeProperties: false)));
        }

        [HttpPost]
        [ActionName("ContactVendor")]
        [ValidateCaptcha("captchaValid")]
        public virtual async Task<IActionResult> ContactVendorSend(ContactVendorModel model, bool captchaValid)
        {
            if (!_vendorSettings.AllowCustomersToContactVendors)
            {
                return ((ControllerBase)(object)this).RedirectToRoute("Homepage");
            }
            Vendor vendor = await _vendorService.GetVendorByIdAsync(model.VendorId);
            if (vendor == null || !vendor.Active || vendor.Deleted)
            {
                return ((ControllerBase)(object)this).RedirectToRoute("Homepage");
            }
            if (_captchaSettings.Enabled && _captchaSettings.ShowOnContactUsPage && !captchaValid)
            {
                ModelStateDictionary modelState = ((ControllerBase)(object)this).ModelState;
                modelState.AddModelError("", await _localizationService.GetResourceAsync("Common.WrongCaptchaMessage"));
            }
            model = await _commonModelFactory.PrepareContactVendorModelAsync(model, vendor, excludeProperties: true);
            if (((ControllerBase)(object)this).ModelState.IsValid)
            {
                string subject = (_commonSettings.SubjectFieldOnContactUsForm ? model.Subject : null);
                string body = _htmlFormatter.FormatText(model.Enquiry, stripTags: false, convertPlainTextToHtml: true, allowHtml: false, allowBbCode: false, resolveLinks: false, addNoFollowTag: false);
                IWorkflowMessageService workflowMessageService = _workflowMessageService;
                Vendor vendor2 = vendor;
                await workflowMessageService.SendContactVendorMessageAsync(vendor2, ((BaseEntity)(await _workContext.GetWorkingLanguageAsync())).Id, model.Email.Trim(), model.FullName, subject, body);
                model.SuccessfullySent = true;
                ContactVendorModel contactVendorModel = model;
                contactVendorModel.Result = await _localizationService.GetResourceAsync("ContactVendor.YourEnquiryHasBeenSent");
                return ((Controller)(object)this).View((object?)model);
            }
            return ((Controller)(object)this).View((object?)model);
        }

        public virtual async Task<IActionResult> Sitemap(SitemapPageModel pageModel)
        {
            if (!_sitemapSettings.SitemapEnabled)
            {
                return ((ControllerBase)(object)this).RedirectToRoute("Homepage");
            }
            return ((Controller)(object)this).View((object?)(await _commonModelFactory.PrepareSitemapModelAsync(pageModel)));
        }

        [CheckAccessClosedStore(true)]
        [CheckLanguageSeoCode(true)]
        public virtual async Task<IActionResult> SitemapXml(int? id)
        {
            string text = ((!_sitemapXmlSettings.SitemapXmlEnabled) ? string.Empty : (await _commonModelFactory.PrepareSitemapXmlAsync(id)));
            string siteMap = text;
            return ((ControllerBase)(object)this).Content(siteMap, "text/xml");
        }

        public virtual async Task<IActionResult> SetStoreTheme(string themeName, string returnUrl = "")
        {
            await _themeContext.SetWorkingThemeNameAsync(themeName);
            if (string.IsNullOrEmpty(returnUrl))
            {
                returnUrl = ((ControllerBase)(object)this).Url.RouteUrl("Homepage");
            }
            if (!((ControllerBase)(object)this).Url.IsLocalUrl(returnUrl))
            {
                returnUrl = ((ControllerBase)(object)this).Url.RouteUrl("Homepage");
            }
            return ((ControllerBase)(object)this).Redirect(returnUrl);
        }

        [HttpPost]
        [CheckAccessClosedStore(true)]
        [CheckAccessPublicStore(true)]
        public virtual async Task<IActionResult> EuCookieLawAccept()
        {
            if (!_storeInformationSettings.DisplayEuCookieLawWarning)
            {
                return ((Controller)(object)this).Json((object?)new
                {
                    stored = false
                });
            }
            Store store = await _storeContext.GetCurrentStoreAsync();
            IGenericAttributeService genericAttributeService = _genericAttributeService;
            await genericAttributeService.SaveAttributeAsync((BaseEntity)(object)(await _workContext.GetCurrentCustomerAsync()), NopCustomerDefaults.EuCookieLawAcceptedAttribute, value: true, ((BaseEntity)store).Id);
            return ((Controller)(object)this).Json((object?)new
            {
                stored = true
            });
        }

        [CheckAccessClosedStore(true)]
        [CheckAccessPublicStore(true)]
        [CheckLanguageSeoCode(true)]
        public virtual async Task<IActionResult> RobotsTextFile()
        {
            return ((ControllerBase)(object)this).Content(await _commonModelFactory.PrepareRobotsTextFileAsync(), MimeTypes.TextPlain);
        }

        public virtual IActionResult GenericUrl()
        {
            return InvokeHttp404();
        }

        [CheckAccessClosedStore(true)]
        public virtual IActionResult StoreClosed()
        {
            return ((Controller)(object)this).View();
        }

        public virtual IActionResult InternalRedirect(string url, bool permanentRedirect)
        {
            if (((ControllerBase)(object)this).HttpContext.Items["nop.RedirectFromGenericPathRoute"] == null || !Convert.ToBoolean(((ControllerBase)(object)this).HttpContext.Items["nop.RedirectFromGenericPathRoute"]))
            {
                url = ((ControllerBase)(object)this).Url.RouteUrl("Homepage");
                permanentRedirect = false;
            }
            if (string.IsNullOrEmpty(url))
            {
                url = ((ControllerBase)(object)this).Url.RouteUrl("Homepage");
                permanentRedirect = false;
            }
            if (!((ControllerBase)(object)this).Url.IsLocalUrl(url))
            {
                url = ((ControllerBase)(object)this).Url.RouteUrl("Homepage");
                permanentRedirect = false;
            }
            if (permanentRedirect)
            {
                return ((ControllerBase)(object)this).RedirectPermanent(url);
            }
            return ((ControllerBase)(object)this).Redirect(url);
        }

        private IList<SelectListItem> PrepareLocationModel()
        {
            List<SelectListItem> list = new List<SelectListItem>();
            list.Add(new SelectListItem
            {
                Text = "Vancouver, Canada (Default Location)",
                Value = "1"
            });
            list.Add(new SelectListItem
            {
                Text = "London, UK",
                Value = "2"
            });
            list.Add(new SelectListItem
            {
                Text = "Sydney, Australia",
                Value = "3"
            });
            list.Add(new SelectListItem
            {
                Text = "San Antonio, TX, USA",
                Value = "4"
            });
            list.Add(new SelectListItem
            {
                Text = "Mumbai, India",
                Value = "5"
            });
            list.Add(new SelectListItem
            {
                Text = "São Paulo, Brazil",
                Value = "6"
            });
            list.Add(new SelectListItem
            {
                Text = "Hong Kong, China",
                Value = "7"
            });
            return list;
        }

        private async Task<string> DownloadScreenshot(string url)
        {
            url = url.Replace("/api/2.0/", "/").Replace("/reports/", "/reports/test/");
            url = HttpUtility.UrlEncode(url);
            string requestUrl = "https://shot.screenshotapi.net/screenshot?token=W36EY7T-ZQJ4CZS-MVDN815-NS8BG2J&url=" + url + "&width=1873&height=961&output=json&file_type=png&wait_for_event=load&block_tracking=true&js=jQuery('.cookiebanner-allow').click()%3B&clip%5Bx%5D)=185&clip%5By%5D)=110&clip%5Bheight%5D)=780&clip%5Bwidth%5D)=1320&thumbnail_width=800";
            HttpClient hc = new HttpClient();
            string response = await hc.GetStringAsync(requestUrl);
            try
            {
                dynamic responseJson = JsonConvert.DeserializeObject(response);
                dynamic screenshotUrl = responseJson.screenshot;
                return screenshotUrl;
            }
            catch (Exception)
            {
                return string.Empty;
            }
        }

        private IList<string> PrepareSuggestionModel(SpeedReport report)
        {
            List<string> list = new List<string>();
            if (report.FullyLoadedTime >= 3500)
            {
                list.Add("The page takes around **" + ((double)report.FullyLoadedTime / 1000.0).ToString("0.00") + " seconds** to fully load. The best practice is to have an average load time of under 3s.");
            }
            if (report.TimeToFirstByte >= 700)
            {
                list.Add($"The server response time is **{report.TimeToFirstByte} ms**. An ideal server response time is 500ms. Please consider installing a caching plugin. In some cases, upgrading to a better hosting server may also help.");
            }
            if (report.PageBytes >= 2097152)
            {
                list.Add("The page size is **" + ((double)report.PageBytes / 1048576.0).ToString("0.00") + " MB**. The ideal page size should be less than 2MB. Please try to resize and compress your media content (image, video, etc), remove unused CSS and unused JavaScript.");
            }
            if (report.PageRequests >= 90)
            {
                list.Add($"The page contain **{report.PageRequests}** file requests, an ideal number would be around 80 file requests. This can be achieved by lazy-loading \"below-the-fold\" resources.");
            }
            IList<SpeedDiagnosticResultModel.HarModel> list2 = JsonConvert.DeserializeObject<IList<SpeedDiagnosticResultModel.HarModel>>(report.Har);
            int num = 0;
            int num2 = 0;
            int num3 = 0;
            List<SpeedDiagnosticResultModel.HarModel> list3 = new List<SpeedDiagnosticResultModel.HarModel>();
            List<SpeedDiagnosticResultModel.HarModel> list4 = new List<SpeedDiagnosticResultModel.HarModel>();
            foreach (SpeedDiagnosticResultModel.HarModel item in list2)
            {
                if (item.Url.Contains(".js"))
                {
                    num += item.Size;
                }
                if (item.Url.Contains(".css"))
                {
                    num2 += item.Size;
                }
                if (item.Url.Contains(".png") || item.Url.Contains(".jpg") || item.Url.Contains(".svg"))
                {
                    num3 += item.Size;
                    if (item.Size >= 102400)
                    {
                        list3.Add(item);
                    }
                }
                if ((double)item.Time >= 600.0 && item != list2.FirstOrDefault())
                {
                    list4.Add(item);
                }
            }
            if (num >= 50000)
            {
                list.Add($"The page has **{num / 1024} KB of JS**. Having a large amount of JavaScript on your site will put a huge burden on the rendering of the page, which in turn hurts the load time. The most common method is to selectively delay JavaScript that does not take part in rendering the \"above-the-fold\" elements.");
            }
            if (num2 >= 50000)
            {
                list.Add($"The page has **{num2 / 1024}KB of CSS**. Reducing unused rules from stylesheets and implementing \"Critical CSS\" can hugely improve the overall page load speed. We have our own dedicated plugin that deals with this issue.");
            }
            if (num3 >= 1500000)
            {
                list.Add($"The page has **{num3 / 1048576} MB of images** that need to be loaded. Having too many images on your site will put a huge burden on the rendering of the page, which in turn hurts the load time. The most common method is to lazy-loading offscreen and hidden images that do not take part in rendering the \"above the fold\" elements.");
            }
            if (list3.Any())
            {
                list.Add($"An images is having a size of above **{list3.FirstOrDefault().Size / 1024} KB**. It's usually suggested that any image does not exceed the size of 100 KB. Consider compressiong / resizing the image to redice its size.");
            }
            return list;
        }

        public virtual async Task<IActionResult> SpeedDiagnostic(int? jobId)
        {
            SpeedDiagnosticModel model = new SpeedDiagnosticModel();
            if (jobId.HasValue && jobId > 0)
            {
                ISpeedDiagnosticService speedDiagnosticService = EngineContext.Current.Resolve<ISpeedDiagnosticService>((IServiceScope)null);
                model.Error = (await speedDiagnosticService.GetJobQueueByIdAsync(jobId.Value)).ErrorMessage;
            }
            model.AvailableLocation = PrepareLocationModel();
            return ((Controller)(object)this).View((object?)model);
        }

        [HttpPost]
        [ActionName("SpeedDiagnostic")]
        public virtual async Task<IActionResult> SpeedDiagnosticSend(SpeedDiagnosticModel model)
        {
            ISpeedDiagnosticService speedDiagnosticService = EngineContext.Current.Resolve<ISpeedDiagnosticService>((IServiceScope)null);
            if (!string.IsNullOrWhiteSpace(model.Url))
            {
                if (!Uri.TryCreate(model.Url, UriKind.Absolute, out Uri uriResult) || (!(uriResult.Scheme == Uri.UriSchemeHttp) && !(uriResult.Scheme == Uri.UriSchemeHttps)))
                {
                    model.Error = "Invalid site url";
                    model.AvailableLocation = PrepareLocationModel();
                    return ((Controller)(object)this).View((object?)model);
                }
                SpeedReportJobQueue job = new SpeedReportJobQueue
                {
                    SiteUrl = model.Url,
                    Location = model.LocationId,
                    Status = "Pending",
                    CreatedOnUtc = DateTime.UtcNow
                };
                await speedDiagnosticService.InsertSpeedReportJobQueueAsync(job);
                return ((ControllerBase)(object)this).RedirectToRoute("CheckSpeedDiagnostic", (object?)new
                {
                    JobGuid = job.JobGuid.ToString()
                });
            }
            model.Error = "Empty site url";
            model.AvailableLocation = PrepareLocationModel();
            return ((Controller)(object)this).View((object?)model);
        }

        public virtual async Task<IActionResult> SpeedDiagnosticResult(string jobGuid)
        {
            if (!string.IsNullOrWhiteSpace(jobGuid))
            {
                ISpeedDiagnosticService speedDiagnosticService = EngineContext.Current.Resolve<ISpeedDiagnosticService>((IServiceScope)null);
                (bool success, string report) result = await speedDiagnosticService.GetGTMetrixResultAsync(jobGuid);
                SpeedDiagnosticResultModel model = new SpeedDiagnosticResultModel();
                if (result.success)
                {
                    return ((ControllerBase)(object)this).RedirectToAction("SpeedDiagnosticSuggestedSolutionMarkDown", (object?)new { jobGuid });
                }
                model.Error = result.report;
                return ((Controller)(object)this).View((object?)model);
            }
            return ((ControllerBase)(object)this).RedirectToRoute("SpeedDiagnostic");
        }

        [HttpPost]
        [ActionName("SpeedDiagnosticResult")]
        public virtual async Task<IActionResult> SpeedDiagnosticResultSend(SpeedDiagnosticResultModel model)
        {
            ISpeedDiagnosticService speedDiagnosticService = EngineContext.Current.Resolve<ISpeedDiagnosticService>((IServiceScope)null);
            if (CommonHelper.IsValidEmail(model.CustomerSuggestedSolution.Email))
            {
                SpeedReportJobQueue job = await speedDiagnosticService.GetJobQueueByIdAsync(model.JobId);
                job.Email = model.CustomerSuggestedSolution.Email;
                await speedDiagnosticService.UpdateSpeedReportJobQueueAsync(job);
                return ((ControllerBase)(object)this).RedirectToRoute("SpeedDiagnosticSuggestedSolution", (object?)new
                {
                    jobGuid = model.JobGuid
                });
            }
            return ((Controller)(object)this).View((object?)model);
        }

        public virtual async Task<IActionResult> SpeedDiagnosticSuggestedSolution(string jobGuid)
        {
            ISpeedDiagnosticService speedDiagnosticService = EngineContext.Current.Resolve<ISpeedDiagnosticService>((IServiceScope)null);
            SpeedReportJobQueue job = await speedDiagnosticService.GetSpeedReportJobQueueByGuidAsync(new Guid(jobGuid));
            if (job != null)
            {
                SpeedReport report = speedDiagnosticService.GetSpeedReportByJobIdAsync(((BaseEntity)job).Id);
                if (report != null)
                {
                    return ((Controller)(object)this).View((object?)new SpeedDiagnosticResultModel
                    {
                        JobGuid = job.JobGuid,
                        Suggestion = PrepareSuggestionModel(report)
                    });
                }
                return ((ControllerBase)(object)this).RedirectToRoute("SpeedDiagnosticResult", (object?)new
                {
                    jobGuid = job.JobGuid
                });
            }
            return ((ControllerBase)(object)this).RedirectToRoute("SpeedDiagnostic");
        }

        public virtual async Task<IActionResult> SpeedDiagnosticSuggestedSolutionMarkDown(string jobGuid)
        {
            ISpeedDiagnosticService speedDiagnosticService = EngineContext.Current.Resolve<ISpeedDiagnosticService>((IServiceScope)null);
            SpeedReportJobQueue job = await speedDiagnosticService.GetSpeedReportJobQueueByGuidAsync(new Guid(jobGuid));
            if (job != null)
            {
                SpeedReport report = speedDiagnosticService.GetSpeedReportByJobIdAsync(((BaseEntity)job).Id);
                if (report != null)
                {
                    SpeedDiagnosticResultModel model = new SpeedDiagnosticResultModel
                    {
                        JobGuid = job.JobGuid,
                        Suggestion = PrepareSuggestionModel(report)
                    };
                    string screenshot = await DownloadScreenshot(job.ReportUrl);
                    StringBuilder overview = new StringBuilder();
                    StringBuilder stringBuilder = overview;
                    StringBuilder stringBuilder2 = stringBuilder;
                    StringBuilder.AppendInterpolatedStringHandler handler = new StringBuilder.AppendInterpolatedStringHandler(26, 1, stringBuilder);
                    handler.AppendLiteral("![Overview](");
                    handler.AppendFormatted(screenshot);
                    handler.AppendLiteral(")\n## Overview\n");
                    stringBuilder2.AppendLine(ref handler);
                    string immediateAction = "";
                    if (report.GtMetrixGrade != "A")
                    {
                        immediateAction = immediateAction + "just having a **Mobile PageSpeed Score of " + report.GtMetrixGrade + "**";
                    }
                    if (!string.IsNullOrWhiteSpace(immediateAction))
                    {
                        immediateAction += " and ";
                    }
                    if (report.FullyLoadedTime > 3500)
                    {
                        immediateAction = immediateAction + "taking **" + ((double)report.FullyLoadedTime / 1000.0).ToString("0.00") + " seconds** to load";
                    }
                    if (!string.IsNullOrWhiteSpace(immediateAction))
                    {
                        immediateAction = "The site requires immediate action as it's " + immediateAction + ". ";
                    }
                    overview.Append(immediateAction);
                    string suggestionJoined = string.Join(" | ", model.Suggestion).ToLowerInvariant();
                    string mainAreasToOptimize2 = "";
                    if (suggestionJoined.Contains("js"))
                    {
                        mainAreasToOptimize2 += "JS, ";
                    }
                    if (suggestionJoined.Contains("css"))
                    {
                        mainAreasToOptimize2 += "CSS, ";
                    }
                    if (suggestionJoined.Contains("css"))
                    {
                        mainAreasToOptimize2 += "images, ";
                    }
                    mainAreasToOptimize2 = mainAreasToOptimize2.Trim().Trim(',');
                    if (!string.IsNullOrWhiteSpace(mainAreasToOptimize2))
                    {
                        stringBuilder = overview;
                        StringBuilder stringBuilder3 = stringBuilder;
                        handler = new StringBuilder.AppendInterpolatedStringHandler(55, 1, stringBuilder);
                        handler.AppendLiteral("In general, the site requires works on the area(s) of ");
                        handler.AppendFormatted(mainAreasToOptimize2);
                        handler.AppendLiteral(".");
                        stringBuilder3.Append(ref handler);
                    }
                    using MemoryStream ms = new MemoryStream();
                    using StreamWriter sw = new StreamWriter(ms);
                    if (!string.IsNullOrWhiteSpace(immediateAction) && !string.IsNullOrWhiteSpace(mainAreasToOptimize2))
                    {
                        sw.Write(overview.ToString());
                        sw.WriteLine();
                        sw.WriteLine();
                    }
                    sw.WriteLine("## Areas to Work On");
                    sw.WriteLine();
                    foreach (string s in model.Suggestion)
                    {
                        sw.WriteLine(s);
                        sw.WriteLine();
                    }
                    sw.Flush();
                    return ((ControllerBase)(object)this).File(ms.ToArray(), "text/plain");
                }
                return ((ControllerBase)(object)this).RedirectToRoute("SpeedDiagnosticResult", (object?)new
                {
                    jobGuid = job.JobGuid
                });
            }
            return ((ControllerBase)(object)this).RedirectToRoute("SpeedDiagnostic");
        }

        public virtual IActionResult CheckSpeedDiagnostic(string jobGuid)
        {
            if (!string.IsNullOrWhiteSpace(jobGuid))
            {
                return ((Controller)(object)this).View("~/Views/Common/CheckSpeedDiagnostic.cshtml", (object?)jobGuid);
            }
            return ((ControllerBase)(object)this).RedirectToRoute("SpeedDiagnostic");
        }

        public async Task<ActionResult> CheckSpeedDiagnosticResult(string jobGuid)
        {
            if (!string.IsNullOrWhiteSpace(jobGuid))
            {
                ISpeedDiagnosticService speedDiagnosticService = EngineContext.Current.Resolve<ISpeedDiagnosticService>((IServiceScope)null);
                return await speedDiagnosticService.CheckResultStatusAsync(jobGuid) switch
                {
                    2 => ((Controller)(object)this).Json((object?)new
                    {
                        Success = false,
                        BackToReport = false
                    }),
                    4 => ((Controller)(object)this).Json((object?)new
                    {
                        Success = false,
                        BackToReport = true
                    }),
                    _ => ((Controller)(object)this).Json((object?)new
                    {
                        Success = true,
                        JobGuid = jobGuid
                    }),
                };
            }
            return ((ControllerBase)(object)this).RedirectToRoute("SpeedDiagnostic");
        }

        public async Task<ActionResult> SetJobQueueCustomerEmail(string jobGuid, string email)
        {
            if (!string.IsNullOrWhiteSpace(jobGuid) && !string.IsNullOrWhiteSpace(email))
            {
                if (CommonHelper.IsValidEmail(email))
                {
                    ISpeedDiagnosticService speedDiagnosticService = EngineContext.Current.Resolve<ISpeedDiagnosticService>((IServiceScope)null);
                    SpeedReportJobQueue job = await speedDiagnosticService.GetSpeedReportJobQueueByGuidAsync(new Guid(jobGuid));
                    if (job != null)
                    {
                        job.Email = email;
                        await speedDiagnosticService.UpdateSpeedReportJobQueueAsync(job);
                        return ((Controller)(object)this).Json((object?)new
                        {
                            Success = true,
                            Message = "Sent successfully."
                        });
                    }
                    return ((Controller)(object)this).Json((object?)new
                    {
                        Success = false,
                        Message = "Empty job."
                    });
                }
                return ((Controller)(object)this).Json((object?)new
                {
                    Success = false,
                    Message = "Invalid email."
                });
            }
            return ((Controller)(object)this).Json((object?)new
            {
                Success = false,
                Message = "Empty email."
            });
        }
    }
}