﻿/* Decompiled from ILSpy */

using Microsoft.AspNetCore.Mvc;

namespace Nop.Web.Controllers
{
    public class HomeController : BasePublicController
    {
        public virtual IActionResult Index()
        {
            return RedirectToRoute("SpeedDiagnostic");
        }
    }
}