﻿using Nop.Web.Framework.Mvc.ModelBinding;
using Nop.Web.Framework.Models;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace Nop.Web.Models.Common
{
    public partial record SpeedDiagnosticModel : BaseNopModel
    {
        #region Ctor

        public SpeedDiagnosticModel()
        {
            AvailableLocation = new List<SelectListItem>();
        }

        #endregion

        #region Properties

        [NopResourceDisplayName("SpeedDignostic.Url")]
        public string Url { get; set; }
        
        [NopResourceDisplayName("SpeedDignostic.Location")]
        public int LocationId { get; set; }

        public IList<SelectListItem> AvailableLocation { get; set; }

        public string Error { get; set; }

        #endregion
    }
}