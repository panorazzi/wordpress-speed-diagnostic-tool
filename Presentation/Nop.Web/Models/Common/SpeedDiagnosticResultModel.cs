﻿using Nop.Web.Framework.Models;
using System.Collections.Generic;
using System;

namespace Nop.Web.Models.Common
{
    public partial record SpeedDiagnosticResultModel : BaseNopModel
    {
        #region Ctor

        public SpeedDiagnosticResultModel()
        {
            Har = new List<HarModel>();
            Suggestion = new List<string>();
            CustomerSuggestedSolution = new SuggestedSolutionModel();
            JobGuid = new Guid();
        }

        #endregion

        #region Properties

        public int ReportId { get; set; }
        public int JobId { get; set; }
        public Guid JobGuid { get; set; }
        public string Gtmetrix_grade { get; set; }
        public string Performance_score { get; set; }
        public string Structure_score { get; set; }
        public string Html_bytes { get; set; }
        public string Page_bytes { get; set; }
        public string Page_requests { get; set; }
        public string Redirect_duration { get; set; }
        public string Connect_duration { get; set; }
        public string Backend_duration { get; set; }
        public string Time_to_first_byte { get; set; }
        public string Fully_loaded_time { get; set; }
        public string Speed_index { get; set; }
        public string Largest_contentful_paint { get; set; }
        public string Time_to_interactive { get; set; }
        public string Total_blocking_time { get; set; }
        public string Cumulative_layout_shift { get; set; }
        public IList<HarModel> Har { get; set; }

        public string Error { get; set; }
        public string SiteUrl { get; set; }
        public string Location { get; set; }
        public IList<string> Suggestion { get; set; }

        public SuggestedSolutionModel CustomerSuggestedSolution { get; set; }
        public bool ShowSuggestedSolution { get; set; }

        #endregion

        #region Nested classes
        public partial record HarModel
        {
            public string Url { get; set; }
            public int Size { get; set; }
            public int Status { get; set; }
            public decimal Time { get; set; }
        }

        public partial record SuggestedSolutionModel
        {
            public string Email { get; set; }
        }
        
        #endregion
    }
}